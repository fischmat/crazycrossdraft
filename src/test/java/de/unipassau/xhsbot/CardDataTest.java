package de.unipassau.xhsbot;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import de.unipassau.xhsbot.model.Card;
import de.unipassau.xhsbot.model.enums.CardType;
import org.json.JSONArray;
import org.junit.Test;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import static junit.framework.TestCase.assertTrue;

/**
 * Created by Manu on 18/12/17.
 */
public class CardDataTest {

    @Test
    public void testGetCardData() throws UnirestException {
        HttpResponse<JsonNode> response = Unirest.get("https://api.hearthstonejson.com/v1/latest/enUS/cards.json")
                .asJson();

        JsonNode body = response.getBody();
        JSONArray array = body.getArray();

        ObjectMapper mapper = new ObjectMapper();

        List<Card> cards = new LinkedList<>();
        array.forEach(c -> {
            try {
                cards.add(mapper.readValue(c.toString(), Card.class));
            } catch (IOException e) {
                assertTrue(false);
            }
        });
    }
}
