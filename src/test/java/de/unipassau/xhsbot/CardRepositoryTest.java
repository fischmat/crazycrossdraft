package de.unipassau.xhsbot;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import de.unipassau.xhsbot.model.Card;
import org.json.JSONArray;
import org.junit.Test;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import static junit.framework.TestCase.assertTrue;

/**
 * Created by Manu on 18/12/17.
 */
public class CardRepositoryTest {

    @Test
    public void testGetCardData() throws UnirestException {
        CardService instance = CardService.getInstance();
        Card card = instance.getCardByName("Pyros");
        assertTrue(card != null);
    }

}
