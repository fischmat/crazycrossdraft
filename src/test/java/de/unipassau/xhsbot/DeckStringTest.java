package de.unipassau.xhsbot;

import com.mashape.unirest.http.exceptions.UnirestException;
import de.unipassau.xhsbot.model.Card;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by Manu on 18/12/17.
 */
public class DeckStringTest {


    @Test
    public void testGenerateDeckString() throws UnirestException {
        CardService service = CardService.getInstance();

        String properString = "AAECAf0EBqsEysECmcICx8cCm9MCotMCDLsClQPmBJYFv8ECwsECwsMCysMCmMQCyMcCltMCzesCAA";

        List<Card> cards1 = new LinkedList<Card>();
        cards1.add(service.getCardByName("Pyros"));
        cards1.add(service.getCardByName("Shimmering Tempest"));
        cards1.add(service.getCardByName("Arcane Intellect"));
        cards1.add(service.getCardByName("Aluneth"));
        cards1.add(service.getCardByName("Frost Lich Jaina"));
        cards1.add(service.getCardByName("Tol'vir Stoneshaper"));
        List<Card> cards2 = new LinkedList<Card>();
        cards2.add(service.getCardByName("Mana Wyrm"));
        cards2.add(service.getCardByName("Flame Geyser"));
        cards2.add(service.getCardByName("Frostbolt"));
        cards2.add(service.getCardByName("Lesser Ruby Spellstone"));
        cards2.add(service.getCardByName("Primordial Glyph"));
        cards2.add(service.getCardByName("Sorcerer's Apprentice"));
        cards2.add(service.getCardByName("Fireball"));
        cards2.add(service.getCardByName("Leyline Manipulator"));
        cards2.add(service.getCardByName("Steam Surger"));
        cards2.add(service.getCardByName("Tar Creeper"));
        cards2.add(service.getCardByName("Servant of Kalimos"));
        cards2.add(service.getCardByName("Blazecaller"));

        String generatedString = DeckString.generateDeckString(cards1, cards2, 2, 637);

        System.out.println(generatedString);

        // Does not work to test because the hashes might be dependant on the order of the cards
//        assertEquals(generatedString, properString);
    }

}
