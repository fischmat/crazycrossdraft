package de.unipassau.xhsbot;

import de.unipassau.xhsbot.session.Session;
import de.unipassau.xhsbot.user.SessionUser;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.User;

import java.io.IOException;
import java.net.URL;

public interface Communicator {

    void respondText(String text, Session session);

    void respondText(String text, Update update);

    void respondText(String text, SessionUser recipient);

    void respondText(String text, User recipient);

    public void respondImage(URL url, String title, Long chatId) throws IOException;

    void respondButtons(Long chatId);
}
