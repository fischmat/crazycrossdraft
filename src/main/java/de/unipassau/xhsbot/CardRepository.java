package de.unipassau.xhsbot;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import de.unipassau.xhsbot.model.Card;
import de.unipassau.xhsbot.model.enums.CardClass;
import de.unipassau.xhsbot.model.enums.CardType;
import info.debatty.java.stringsimilarity.NormalizedLevenshtein;
import org.json.JSONArray;

import java.io.IOException;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class CardRepository {

    private String endpointUri;

    private List<Card> cards;

    public CardRepository() throws UnirestException {
        this("https://api.hearthstonejson.com/v1/latest/enUS/cards.json");
    }

    public CardRepository(String endpointUri) throws UnirestException {
        this.endpointUri = endpointUri;
        _init();
    }

    private void _init() throws UnirestException {
        cards = new LinkedList<>();

        // Init cards by using the endpoint
        HttpResponse<JsonNode> response = Unirest.get(endpointUri).asJson();

        JsonNode body = response.getBody();
        JSONArray array = body.getArray();

        ObjectMapper mapper = new ObjectMapper();

        array.forEach(c -> {
            try {
                cards.add(mapper.readValue(c.toString(), Card.class));
            } catch (IOException e) {
                // TODO log fail
            }
        });

        cards = cards.stream().filter(card -> card.getName() != null).collect(Collectors.toList());
        cards = getCard(CardType.WEAPON, CardType.MINION, CardType.SPELL);
    }

    public void refresh() throws UnirestException {
        _init();
    }

    public List<Card> getCards() {
        return cards;
    }

    public List<Card> getCard(CardClass... cardClass) {
        return cards.stream().filter(c -> c.isOfAnyClass(cardClass)).collect(Collectors.toList());
    }

    public List<Card> getCard(CardType... cardType) {
        return cards.stream().filter(c -> c.hasAnyType(cardType)).collect(Collectors.toList());
    }

    public Card getCardByName(String name) {
        return cards.stream().filter(c -> c.getName().equals(name)).findAny().orElse(null);
    }

    public List<Card> getMostRelevantCards(String name) {
        NormalizedLevenshtein levenshtein = new NormalizedLevenshtein();
        return cards.stream().sorted(new Comparator<Card>() {
            @Override
            public int compare(Card o1, Card o2) {
                return 0;
            }
        }).collect(Collectors.toList()).subList(0, 4);
    }
}
