package de.unipassau.xhsbot.session;

import de.unipassau.xhsbot.CardRepository;
import de.unipassau.xhsbot.CardService;
import de.unipassau.xhsbot.Communicator;
import de.unipassau.xhsbot.model.Card;
import de.unipassau.xhsbot.model.enums.CardClass;
import de.unipassau.xhsbot.user.SessionUser;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.User;

import java.io.IOException;
import java.net.URL;

public class Session {

    private enum SessionState {
        SelectClass, CardSelection
    }

    private SessionState state;

    private SessionUser user1;

    private SessionUser user2;

    public Session(SessionUser user1, SessionUser user2) {
        this.user1 = user1;
        this.user2 = user2;
        this.state = SessionState.SelectClass;
    }

    public SessionUser getUser1() {
        return user1;
    }

    public void setUser1(SessionUser user1) {
        this.user1 = user1;
    }

    public SessionUser getUser2() {
        return user2;
    }

    public void setUser2(SessionUser user2) {
        this.user2 = user2;
    }

    private SessionUser getSender(Update update) {
        User sender = update.getMessage().getFrom();
        if(sender.getUserName().equals(user1.getName())) {
            return user1;
        } else {
            return user2;
        }
    }

    private SessionUser getOtherUser(SessionUser user) {
        if (user.equals(user1)) {
            return user2;
        } else {
            return user1;
        }
    }

    private CardClass getCardClass(String s) throws IllegalArgumentException {
        switch (s.toLowerCase()) {
            case "deathknight": return CardClass.DEATHKNIGHT;
            case "dream": return CardClass.DREAM;
            case "druid": return CardClass.DRUID;
            case "hunter": return CardClass.HUNTER;
            case "invalid": return CardClass.INVALID;
            case "mage": return CardClass.MAGE;
            case "neutral": return CardClass.NEUTRAL;
            case "paladin": return CardClass.PALADIN;
            case "priest": return CardClass.PRIEST;
            case "rogue": return CardClass.ROGUE;
            case "shaman": return CardClass.SHAMAN;
            case "warlock": return CardClass.WARLOCK;
            case "warrior": return CardClass.WARRIOR;
            default: throw new IllegalArgumentException("Hm, I don't know such a class...");
        }
    }

    private void forward(Update update, Communicator communicator) {
        String senderName = update.getMessage().getFrom().getUserName();

        if(!user1.getName().equals(senderName)) {
            communicator.respondText(user2.getName() + ": " + update.getMessage().getText(), user1);
        }
        if(!user2.getName().equals(senderName)) {
            communicator.respondText(user1.getName() + ": " + update.getMessage().getText(), user2);
        }
    }

    public void handoff(Communicator communicator) {
        if(state == SessionState.SelectClass) {
            communicator.respondText("Please select a class.", this);
        }
    }

    public void handleMessage(Update update, Communicator communicator) {
        try {
            String messageText = update.getMessage().getText();
            SessionUser sender = getSender(update);

            // Has a class been chosen?
            if(state == SessionState.SelectClass) {
                CardClass cardClass = getCardClass(messageText);
                sender.setCardClass(cardClass);
                communicator.respondText(sender.getName() + " picks " + cardClass, this);

                if(user1.getCardClass() != null && user2.getCardClass() != null) {
                    state = SessionState.CardSelection;
                    communicator.respondText("Ok everyone, we're good to go... Please select five cards.", this);
                    communicator.respondButtons(user1.getChatId());
                }

            } else if(state == SessionState.CardSelection) {
                SessionUser otherUser = getOtherUser(sender);

                if(otherUser.getCards().size() <= 5) {
                    CardRepository cardRepo = CardService.getInstance().getCardRepository();
                    Card card = cardRepo.getCardByName(messageText);

                    if(card != null) {
                        otherUser.addCard(card);
                        try {
                            communicator.respondImage(new URL("http://media.services.zam.com/v1/media/byName/hs/cards/enus/" + card.getId() + ".png"), card.getName(), sender.getChatId());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    } else {
                        communicator.respondText("There is no card called " + messageText, sender);
                    }

                } else if(sender.getCards().size() <= 5) {
                    communicator.respondText("You already picked five cards", sender);

                } else {
                    communicator.respondText("Ok, we're done. Here is your hash: " + user1.computeCardHash(), user1);
                    communicator.respondText("Ok, we're done. Here is your hash: " + user2.computeCardHash(), user1);
                }
            }

            forward(update, communicator);
        } catch (IllegalArgumentException e) {
            communicator.respondText(e.getMessage(), update);
        }
    }
}
