package de.unipassau.xhsbot.session;

import de.unipassau.xhsbot.user.SessionUser;

import java.util.Collection;
import java.util.HashSet;

public class SessionManager {

    private Collection<Session> sessions = new HashSet<Session>();

    private static SessionManager instance = new SessionManager();

    public static SessionManager getInstance() {
        return instance;
    }



    public void registerSession(Session session) {
        sessions.add(session);
    }

    public Session getUserSession(SessionUser user) {
        for (Session session : sessions) {
            if(session.getUser1().getName().equals(user.getName()) ||
                    session.getUser2().getName().equals(user.getName())) {
                return session;
            }
        }
        return null;
    }
}
