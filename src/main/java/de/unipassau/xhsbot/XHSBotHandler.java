package de.unipassau.xhsbot;

import de.unipassau.xhsbot.session.Session;
import de.unipassau.xhsbot.session.SessionManager;
import de.unipassau.xhsbot.user.SessionUser;
import de.unipassau.xhsbot.user.UserManager;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.methods.send.SendPhoto;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.User;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class XHSBotHandler extends TelegramLongPollingBot implements Communicator {

    public void onUpdateReceived(Update update) {
        // We check if the update has a message and the message has text
        if (update.hasMessage() && update.getMessage().hasText()) {
            UserManager userManager = UserManager.getInstance();
            SessionManager sessionManager = SessionManager.getInstance();

            String messageText = update.getMessage().getText();
            User sender = update.getMessage().getFrom();

            // Check if this user just joined:
            if(!userManager.isRegistered(sender)) {
                welcomeUser(sender, update);
            } else if(messageText.equals("/whosthere")) {
                listUsers(sender);
            } else if(messageText.startsWith("/join ")) {
                joinSession(sender, messageText);
            } else {
                SessionUser user = userManager.get(sender.getUserName());

                Session session = sessionManager.getUserSession(user);
                session.handleMessage(update, this);
            }
        }
    }

    private void welcomeUser(User sender, Update update) {
        UserManager userManager = UserManager.getInstance();

        if(sender.getUserName() == null || sender.getUserName().isEmpty()) {
            respondText("Hi! Please tell me your name with /name", sender);
        } else {
            userManager.registerUser(sender, update.getMessage().getChatId());
            respondText("Hi " + sender.getUserName() + "! Join a session with /join", sender);
        }
    }

    private void listUsers(User sender) {
        UserManager userManager = UserManager.getInstance();

        StringBuilder response = new StringBuilder("These people are here:\n");
        for (SessionUser user : userManager.getUsers()) {
            response.append(user.getName()).append("\n");
        }
        respondText(response.toString(), sender);
    }

    private void joinSession(User sender, String messageText) {
        UserManager userManager = UserManager.getInstance();
        SessionManager sessionManager = SessionManager.getInstance();

        // First check if this guy already has a name:
        if(!userManager.isRegistered(sender)) {
            respondText("Please tell me your name before with /name", sender);
        }

        String partnerName = messageText.substring("/join ".length());

        // Check partner name known:
        if(!userManager.isRegistered(partnerName)) {
            respondText("I don't know " + partnerName, sender);
            return;
        }

        Session session = new Session(userManager.get(sender.getUserName()), userManager.get(partnerName));
        sessionManager.registerSession(session);
        session.handoff(this);
        respondText("You are now connected " + sender.getUserName() + " and " + partnerName + "!", session);
    }

    public void respondText(String text, Session session) {
        respondText(text, session.getUser1());
        respondText(text, session.getUser2());
    }

    public void respondText(String text, Update update) {
        respondText(text, update.getMessage().getChatId());
    }

    public void respondText(String text, SessionUser recipient) {
        respondText(text, recipient.getChatId());
    }

    public void respondText(String text, User recipient) {
        UserManager manager = UserManager.getInstance();
        respondText(text, manager.get(recipient.getUserName()).getChatId());
    }

    public void respondText(String text, Long chatId) {
        SendMessage message = new SendMessage() // Create a SendMessage object with mandatory fields
                .setChatId(chatId)
                .setText(text);
        try {
            execute(message); // Call method to send the message
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    public void respondImage(URL url, String title, Long chatId) throws IOException {
        SendPhoto photo = new SendPhoto()
                .setNewPhoto(title, url.openStream())
                .setChatId(chatId);

        try {
            sendPhoto(photo);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    public void respondButtons(Long chatId) {
        SendMessage message = new SendMessage() // Create a message object object
                .setChatId(chatId)
                .setText("You send /start");
        InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();

        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
        for (int i = 1; i <= 5; i++) {
            List<InlineKeyboardButton> rowInline = new ArrayList<>();
            rowInline.add(new InlineKeyboardButton().setText("Card " + i).setCallbackData("update_msg_text" + i));
            rowInline.add(new InlineKeyboardButton().setText("View").setCallbackData("view" + i));
            // Set the keyboard to the markup
            rowsInline.add(rowInline);
        }
        // Add it to the message
        markupInline.setKeyboard(rowsInline);
        message.setReplyMarkup(markupInline);
        try {
            execute(message); // Sending our message object to user
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    public String getBotUsername() {
        return "crazycrossdraft";
    }

    public String getBotToken() {
        return "401301342:AAGPuZEviL5ckFlaplEopttfTtobqCd-Lgs";
    }
}
