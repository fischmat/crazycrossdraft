package de.unipassau.xhsbot;

import de.unipassau.xhsbot.model.Card;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.List;

/**
 * Created by Manu on 18/12/17.
 */
public class DeckString {

    public static void writeVarInt(DataOutputStream dos, int value) throws IOException {
        //taken from http://wiki.vg/Data_types, altered slightly
        do {
            byte temp = (byte) (value & 0b01111111);
            // Note: >>> means that the sign bit is shifted with the rest of the
            // number rather than being left alone
            value >>>= 7;
            if (value != 0) {
                temp |= 0b10000000;
            }
            dos.writeByte(temp);
        } while (value != 0);
    }

    /**
     * format: standard = 2, wild = 1
     * hero: vmtl: Malfurion: 274 Rexxar: 31 Jaina: 637 Uther: 671 Anduin: 813 Valeera: 930 Thrall: 1066 Gul'Dan: 893 Garrosh: 7
     *
     * @param deck1
     * @param deck2
     * @param format
     * @return
     */
    public static String generateDeckString(List<Card> deck1, List<Card> deck2, int format, int dbfHero) {
        ByteArrayOutputStream baos = null;
        DataOutputStream dos = null;

        try {
            baos = new ByteArrayOutputStream();
            dos = new DataOutputStream(baos);

            writeVarInt(dos, 0); // always zero
            writeVarInt(dos, 1); // encoding version number
            writeVarInt(dos, format); // standard = 2, wild = 1
            writeVarInt(dos, 1); // number of heroes in heroes array, always 1
            writeVarInt(dos, dbfHero); // DBF ID of hero
            writeVarInt(dos, deck1.size()); // number of 1-quantity cards

            for (int i = 0; i < deck1.size(); i++) {
                writeVarInt(dos, deck1.get(i).getDbfId());
            }

            writeVarInt(dos, deck2.size()); // number of 2-quantity cards

            for (int i = 0; i < deck2.size(); i++) {
                writeVarInt(dos, deck2.get(i).getDbfId());
            }

            writeVarInt(dos, 0); //the number of cards that have quantity greater than 2. Always 0 for constructed

            dos.flush(); //flushes the output stream to the byte array output stream

            if (baos != null)
                baos.close();
            if (dos != null)
                dos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        String deckString = Base64.getEncoder().encodeToString(baos.toByteArray()); //encode the byte array to a base64 string
        return deckString;
    }
}
