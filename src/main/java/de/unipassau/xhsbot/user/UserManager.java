package de.unipassau.xhsbot.user;

import org.telegram.telegrambots.api.objects.User;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class UserManager {

    private Map<String, SessionUser> users = new HashMap<>();

    private static UserManager instance = new UserManager();

    public static UserManager getInstance() {
        return instance;
    }

    public void registerUser(User user, Long chatId) {
        users.put(user.getUserName(), new SessionUser(user.getUserName(), chatId));
    }

    public boolean isRegistered(String userName) {
        return users.containsKey(userName);
    }

    public boolean isRegistered(User user) {
        return users.containsKey(user.getUserName());
    }

    public SessionUser get(String name) {
        return users.get(name);
    }

    public Collection<SessionUser> getUsers() {
        return users.values();
    }
}
