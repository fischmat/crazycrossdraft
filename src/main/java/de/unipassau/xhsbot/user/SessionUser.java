package de.unipassau.xhsbot.user;

import de.unipassau.xhsbot.model.Card;
import de.unipassau.xhsbot.model.enums.CardClass;
import org.telegram.telegrambots.api.objects.User;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class SessionUser extends User {

    private String name;

    private Long chatId;

    private CardClass cardClass;

    private List<Card> cards = new ArrayList<>();

    public SessionUser(String name, Long chatId) {
        this.name = name;
        this.chatId = chatId;
    }

    public Long getChatId() {
        return chatId;
    }

    public void setChatId(Long chatId) {
        this.chatId = chatId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addCard(Card card) {
        cards.add(card);
    }

    public CardClass getCardClass() {
        return cardClass;
    }

    public void setCardClass(CardClass cardClass) {
        this.cardClass = cardClass;
    }

    public Collection<Card> getCards() {
        return cards;
    }

    public void setCards(List<Card> cards) {
        this.cards = cards;
    }

    public String computeCardHash() {
        return "abcdefghijk==";
    }
}
