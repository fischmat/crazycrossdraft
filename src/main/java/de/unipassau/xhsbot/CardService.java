package de.unipassau.xhsbot;

import com.mashape.unirest.http.exceptions.UnirestException;
import de.unipassau.xhsbot.model.Card;
import de.unipassau.xhsbot.model.enums.CardClass;
import de.unipassau.xhsbot.model.enums.CardType;

import java.util.List;

public class CardService {
    private static CardService ourInstance = new CardService();

    private CardRepository cardRepository;

    public static CardService getInstance() {
        return ourInstance;
    }

    private CardService() {
        _init();
    }

    private void _init() {
        try {
            cardRepository = new CardRepository();
        } catch (UnirestException e) {
            throw new RuntimeException(e);
        }
    }

    public CardRepository getCardRepository() {
        return cardRepository;
    }

    public List<Card> getCards() {
        return cardRepository.getCards();
    }

    public List<Card> getCard(CardClass... cardClass) {
        return cardRepository.getCard(cardClass);
    }

    public List<Card> getCard(CardType... cardType) {
        return cardRepository.getCard(cardType);
    }

    public Card getCardByName(String name) {
        return cardRepository.getCardByName(name);
    }
}
