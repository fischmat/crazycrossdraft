package de.unipassau.xhsbot.model.enums;

public enum CardClass {
    DEATHKNIGHT(1),
    DREAM(11),
    DRUID(2),
    HUNTER(3),
    INVALID(0),
    MAGE(4),
    NEUTRAL(12),
    PALADIN(5),
    PRIEST(6),
    ROGUE(7),
    SHAMAN(8),
    WARLOCK(9),
    WARRIOR(10);

    private int id;

    CardClass(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
