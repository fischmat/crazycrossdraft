package de.unipassau.xhsbot.model.enums;

public enum CardType {
    ABILITY(5),
    ENCHANTMENT(6),
    GAME(1),
    HERO(3),
    HERO_POWER(10),
    INVALID(0),
    ITEM(8),
    MINION(4),
    PLAYER(2),
    SPELL(5),
    TOKEN(9),
    WEAPON(7);

    private int id;

    CardType(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
