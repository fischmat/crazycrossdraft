package de.unipassau.xhsbot.model;

import com.fasterxml.jackson.annotation.*;
import de.unipassau.xhsbot.model.enums.CardClass;
import de.unipassau.xhsbot.model.enums.CardType;

import java.util.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "dbfId",
        "name",
        "text",
        "flavor",
        "artist",
        "attack",
        "cardClass",
        "collectible",
        "cost",
        "elite",
        "faction",
        "health",
        "mechanics",
        "rarity",
        "set",
        "type"
})
public class Card {

    @JsonProperty("id")
    private String id;
    @JsonProperty("dbfId")
    private Integer dbfId;
    @JsonProperty("name")
    private String name;
    @JsonProperty("text")
    private String text;
    @JsonProperty("flavor")
    private String flavor;
    @JsonProperty("artist")
    private String artist;
    @JsonProperty("attack")
    private Integer attack;
    @JsonProperty("cardClass")
    private CardClass cardClass;
    @JsonProperty("collectible")
    private Boolean collectible;
    @JsonProperty("cost")
    private Integer cost;
    @JsonProperty("elite")
    private Boolean elite;
    @JsonProperty("faction")
    private String faction;
    @JsonProperty("health")
    private Integer health;
    @JsonProperty("mechanics")
    private List<String> mechanics = null;
    @JsonProperty("rarity")
    private String rarity;
    @JsonProperty("set")
    private String set;
    @JsonProperty("type")
    private CardType type;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("dbfId")
    public Integer getDbfId() {
        return dbfId;
    }

    @JsonProperty("dbfId")
    public void setDbfId(Integer dbfId) {
        this.dbfId = dbfId;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("text")
    public String getText() {
        return text;
    }

    @JsonProperty("text")
    public void setText(String text) {
        this.text = text;
    }

    @JsonProperty("flavor")
    public String getFlavor() {
        return flavor;
    }

    @JsonProperty("flavor")
    public void setFlavor(String flavor) {
        this.flavor = flavor;
    }

    @JsonProperty("artist")
    public String getArtist() {
        return artist;
    }

    @JsonProperty("artist")
    public void setArtist(String artist) {
        this.artist = artist;
    }

    @JsonProperty("attack")
    public Integer getAttack() {
        return attack;
    }

    @JsonProperty("attack")
    public void setAttack(Integer attack) {
        this.attack = attack;
    }

    @JsonProperty("cardClass")
    public CardClass getCardClass() {
        return cardClass;
    }

    @JsonProperty("cardClass")
    public void setCardClass(CardClass cardClass) {
        this.cardClass = cardClass;
    }

    @JsonProperty("collectible")
    public Boolean getCollectible() {
        return collectible;
    }

    @JsonProperty("collectible")
    public void setCollectible(Boolean collectible) {
        this.collectible = collectible;
    }

    @JsonProperty("cost")
    public Integer getCost() {
        return cost;
    }

    @JsonProperty("cost")
    public void setCost(Integer cost) {
        this.cost = cost;
    }

    @JsonProperty("elite")
    public Boolean getElite() {
        return elite;
    }

    @JsonProperty("elite")
    public void setElite(Boolean elite) {
        this.elite = elite;
    }

    @JsonProperty("faction")
    public String getFaction() {
        return faction;
    }

    @JsonProperty("faction")
    public void setFaction(String faction) {
        this.faction = faction;
    }

    @JsonProperty("health")
    public Integer getHealth() {
        return health;
    }

    @JsonProperty("health")
    public void setHealth(Integer health) {
        this.health = health;
    }

    @JsonProperty("mechanics")
    public List<String> getMechanics() {
        return mechanics;
    }

    @JsonProperty("mechanics")
    public void setMechanics(List<String> mechanics) {
        this.mechanics = mechanics;
    }

    @JsonProperty("rarity")
    public String getRarity() {
        return rarity;
    }

    @JsonProperty("rarity")
    public void setRarity(String rarity) {
        this.rarity = rarity;
    }

    @JsonProperty("set")
    public String getSet() {
        return set;
    }

    @JsonProperty("set")
    public void setSet(String set) {
        this.set = set;
    }

    @JsonProperty("type")
    public CardType getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(CardType type) {
        this.type = type;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public boolean hasAnyType(CardType... types) {
        return Arrays.stream(types).anyMatch(t -> t.equals(type));
    }

    public boolean isOfAnyClass(CardClass... cardClasses) {
        return Arrays.stream(cardClasses).anyMatch(t -> t.equals(cardClass));
    }

    @Override
    public String toString() {
        return "Card{" +
                "name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Card)) return false;

        Card card = (Card) o;

        if (!id.equals(card.id)) return false;
        return dbfId != null ? dbfId.equals(card.dbfId) : card.dbfId == null;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + (dbfId != null ? dbfId.hashCode() : 0);
        return result;
    }
}